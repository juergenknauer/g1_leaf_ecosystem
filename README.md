# Online Repository for the study Knauer et al., Global Change Biology 2017 #

This is an accompanying online repository for the study:

Knauer J, Zaehle S, Medlyn BE, et al.: 
"Towards physiologically meaningful water-use efficiency estimates from eddy covariance data",
Global Change Biology. 2017, 00:1-17. https://doi.org/10.1111/gcb.13893

All calculation steps performed in the study can be reproduced with the 'bigleaf' R-package.
The package is on CRAN and can be installed using

install.packages("bigleaf")

for further information on the package, see https://bitbucket.org/juergenknauer/bigleaf


for questions or feedback, please email jknauer@bgc-jena.mpg.de
